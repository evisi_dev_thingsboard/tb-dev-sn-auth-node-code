/**
 * Copyright © 2020 The Thingsboard Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.rule.engine.node.enrichment;

import com.google.common.hash.Hashing;
import lombok.extern.slf4j.Slf4j;
import net.solarnetwork.web.security.AuthorizationV2Builder;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.thingsboard.rule.engine.api.RuleNode;
import org.thingsboard.rule.engine.api.TbContext;
import org.thingsboard.rule.engine.api.TbNode;
import org.thingsboard.rule.engine.api.TbNodeConfiguration;
import org.thingsboard.rule.engine.api.TbNodeException;
import org.thingsboard.rule.engine.api.util.TbNodeUtils;
import org.thingsboard.server.common.data.plugin.ComponentType;
import org.thingsboard.server.common.msg.TbMsg;
import org.thingsboard.server.common.msg.TbMsgMetaData;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Date;
import java.util.Map;

import static org.thingsboard.rule.engine.api.TbRelationTypes.SUCCESS;

@Slf4j
@RuleNode(
        type = ComponentType.ENRICHMENT,
        name = "solar network auth",
        configClazz = SolarNetworkAuthNodeConfiguration.class,
        nodeDescription = "Creating Solar Network headers, and adding them to metadata.",
        nodeDetails = "",
        uiResources = {"static/rulenode/custom-nodes-config.js"},
        configDirective = "enrichmentNodeSolarNetworkAuthConfig")
public class SolarNetworkAuthNode implements TbNode {
    private static final String ACCEPT_HEADER = "Accept";
    private static final String DATE_HEADER = "X-SN-Date";
    private static final String AUTH_HEADER = "Authorization";
    private static final String DIGEST_HEADER = "Digest";
    private static final String CONTENT_TYPE_HEADER = "Content-Type";
    private static final String CONTENT_TYPE_HEADER_VALUE = "application/json; charset=UTF-8";

    private SolarNetworkAuthNodeConfiguration config;

    @Override
    public void init(TbContext tbContext, TbNodeConfiguration configuration) throws TbNodeException {
        this.config = TbNodeUtils.convert(configuration, SolarNetworkAuthNodeConfiguration.class);
    }

    @Override
    public void onMsg(TbContext ctx, TbMsg msg) {
        log.debug("Message body: {}", msg.getData());
        final String token = TbNodeUtils.processPattern(config.getUserTokenPattern(), msg.getMetaData());
        final String secret = TbNodeUtils.processPattern(config.getUserSecretPattern(), msg.getMetaData());
        final String host = config.getHost().replaceFirst("^(http[s]?://www\\.|http[s]?://|www\\.)", "");
        final String path = config.getService();
        final byte[] msgBodySha256 = Hashing.sha256().hashString(msg.getData(), StandardCharsets.UTF_8).asBytes();
        final String msgBodySha256Base64 = Base64.getEncoder().encodeToString(msgBodySha256);
        final String digest = "sha-256=" + msgBodySha256Base64;
        final Date date = new Date();
        final String httpDate = AuthorizationV2Builder.httpDate(date);

        AuthorizationV2Builder builder = new AuthorizationV2Builder(token);
        builder
                .method(HttpMethod.POST)
                .host(host)
                .path(path)
                .date(date)
                .digest(digest)
                .contentType(CONTENT_TYPE_HEADER_VALUE)
                .header(DATE_HEADER, httpDate)
                .contentSHA256(msgBodySha256)
                .saveSigningKey(secret);

        final String auth = builder.build();

        final Map<String, String> headers = msg.getMetaData().values();
        headers.put(ACCEPT_HEADER, MediaType.APPLICATION_JSON_VALUE);
        headers.put(CONTENT_TYPE_HEADER, CONTENT_TYPE_HEADER_VALUE);
        headers.put(AUTH_HEADER, auth);
        headers.put(DIGEST_HEADER, digest);
        headers.put(DATE_HEADER, httpDate);

        ctx.tellNext(new TbMsg(msg.getId(), msg.getType(), msg.getOriginator(), new TbMsgMetaData(headers), msg.getData(),
                msg.getRuleChainId(), msg.getRuleNodeId(), msg.getClusterPartition()), SUCCESS);
    }

    @Override
    public void destroy() {
    }
}
